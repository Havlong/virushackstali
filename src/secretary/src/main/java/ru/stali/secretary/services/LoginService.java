package ru.stali.secretary.services;

import ru.stali.secretary.transfer.CredentialsDTO;
import ru.stali.secretary.transfer.TokenDTO;

/**
 * 03.05.2020
 * LoginService
 *
 * @author Havlong
 * @version v1.0
 */
public interface LoginService {
    TokenDTO login(CredentialsDTO credentialsDTO);
}
