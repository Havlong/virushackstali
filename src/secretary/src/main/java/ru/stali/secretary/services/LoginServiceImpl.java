package ru.stali.secretary.services;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.stali.secretary.models.Token;
import ru.stali.secretary.models.User;
import ru.stali.secretary.repositories.TokenRepository;
import ru.stali.secretary.repositories.UserRepository;
import ru.stali.secretary.transfer.CredentialsDTO;
import ru.stali.secretary.transfer.TokenDTO;

/**
 * 03.05.2020
 * LoginServiceImpl
 *
 * @author Havlong
 * @version v1.0
 */
@Service
public class LoginServiceImpl implements LoginService {

    private final TokenRepository tokenRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    public LoginServiceImpl(TokenRepository tokenRepository, PasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.tokenRepository = tokenRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    @Override
    public TokenDTO login(CredentialsDTO credentialsDTO) {
        User userCandidate = userRepository.findUserByUserName(credentialsDTO.getLogin());

        if (userCandidate == null) {
            throw new IllegalArgumentException("User not found");
        }
        if (passwordEncoder.matches(credentialsDTO.getPassword(), userCandidate.getHashPassword())) {
            Token token = Token.builder()
                    .user(userCandidate)
                    .value(RandomStringUtils.random(14, true, true))
                    .build();
            tokenRepository.save(token);
            return TokenDTO.from(token);
        } else {
            throw new IllegalArgumentException("Passwords don't match");
        }
    }
}
