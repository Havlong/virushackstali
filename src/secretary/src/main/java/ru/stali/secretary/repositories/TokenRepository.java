package ru.stali.secretary.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.stali.secretary.models.Token;

/**
 * 03.05.2020
 * TokenRepository
 *
 * @author Havlong
 * @version v1.0
 */
public interface TokenRepository extends MongoRepository<Token, String> {
    Token findTokenByValue(String value);
}
