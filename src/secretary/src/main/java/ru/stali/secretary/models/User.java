package ru.stali.secretary.models;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 03.05.2020
 * User
 *
 * @author Havlong
 * @version v1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@EqualsAndHashCode
@Document
public class User {
    @Id
    private String id;

    private String userName;
    private String hashPassword;
}
