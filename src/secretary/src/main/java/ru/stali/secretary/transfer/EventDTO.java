package ru.stali.secretary.transfer;

import lombok.*;

/**
 * 04.05.2020
 * EventDTO
 *
 * @author Havlong
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class EventDTO {
    private String author;
    private String value;

    public static EventDTO from(MessageDTO messageDTO) {
        return EventDTO.builder().author(messageDTO.getAuthor()).value(messageDTO.getValue()).build();
    }
}
