package ru.stali.secretary.security.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import ru.stali.secretary.security.filters.TokenAuthFilter;

/**
 * 03.05.2020
 * SecurityConfig
 *
 * @author Havlong
 * @version v1.0
 */
@Configuration
@ComponentScan("ru.stali")
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final AuthenticationProvider authenticationProvider;
    private final TokenAuthFilter tokenAuthFilter;

    public SecurityConfig(AuthenticationProvider userDetailsService, TokenAuthFilter tokenAuthFilter) {
        this.authenticationProvider = userDetailsService;
        this.tokenAuthFilter = tokenAuthFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.addFilterBefore(tokenAuthFilter, BasicAuthenticationFilter.class)
                .antMatcher("/**")
                .authenticationProvider(authenticationProvider)
                .authorizeRequests()
                .antMatchers("/signUp").permitAll()
                .antMatchers("/login").permitAll()
                .antMatchers("/stream/**").permitAll()
                .antMatchers("/streamapi/**").permitAll()
                .anyRequest().fullyAuthenticated()
                .and()
                .csrf()
                .disable();
    }
}
