package ru.stali.secretary.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.stali.secretary.repositories.UserRepository;
import ru.stali.secretary.services.SignUpService;
import ru.stali.secretary.transfer.CredentialsDTO;

/**
 * 03.05.2020
 * SignUpController
 *
 * @author Havlong
 * @version v1.0
 */
@RestController
public class SignUpController {
    private final UserRepository userRepository;
    private final SignUpService signUpService;

    public SignUpController(UserRepository userRepository, SignUpService signUpService) {
        this.userRepository = userRepository;
        this.signUpService = signUpService;
    }

    @PostMapping("/signUp")
    public ResponseEntity<String> signUp(@RequestBody CredentialsDTO credentialsDTO) {
        if (userRepository.findUserByUserName(credentialsDTO.getLogin()) != null) {
            return ResponseEntity.badRequest().body("Login already exists");
        }
        return signUpService.signUp(credentialsDTO) ? ResponseEntity.ok("success") : ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("failure");
    }
}
