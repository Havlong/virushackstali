package ru.stali.secretary.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.stali.secretary.services.LoginService;
import ru.stali.secretary.transfer.CredentialsDTO;
import ru.stali.secretary.transfer.TokenDTO;

/**
 * 03.05.2020
 * LoginController
 *
 * @author Havlong
 * @version v1.0
 */
@RestController
public class LoginController {

    final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/login")
    public TokenDTO login(@RequestBody CredentialsDTO credentialsDTO) {
        return loginService.login(credentialsDTO);
    }
}
