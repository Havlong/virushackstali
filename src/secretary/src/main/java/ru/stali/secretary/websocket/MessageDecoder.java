package ru.stali.secretary.websocket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.stali.secretary.transfer.MessageDTO;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

/**
 * 04.05.2020
 * MessageDecoder
 *
 * @author Havlong
 * @version v1.0
 */
public class MessageDecoder implements Decoder.Text<MessageDTO> {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public MessageDTO decode(String s) throws DecodeException {
        try {
            return objectMapper.readValue(s, MessageDTO.class);
        } catch (JsonProcessingException e) {
            throw new DecodeException(s, "Invalid JSON");
        }
    }

    @Override
    public boolean willDecode(String s) {
        return s != null;
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
