package ru.stali.secretary.services;

import ru.stali.secretary.models.Stream;
import ru.stali.secretary.transfer.EventDTO;

import javax.websocket.Session;
import java.util.List;

/**
 * 04.05.2020
 * StreamService
 *
 * @author Havlong
 * @version v1.0
 */
public interface StreamService {
    List<Session> getSessionsForStream(Stream stream);

    void registerSessionForStream(Stream stream, Session session);

    void closeSessionForStream(Stream stream, Session session);

    void sendMessageToStream(Stream stream, EventDTO eventDTO);

    void startStream(Stream stream);

    void closeStream(Stream stream);
}
