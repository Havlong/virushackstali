package ru.stali.secretary.models;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 03.05.2020
 * Token
 *
 * @author Havlong
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
@EqualsAndHashCode
@Document
public class Token {
    @Id
    private String id;

    private String value;

    @DBRef
    private User user;
}
