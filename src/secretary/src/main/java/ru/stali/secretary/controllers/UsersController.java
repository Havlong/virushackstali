package ru.stali.secretary.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.stali.secretary.repositories.UserRepository;
import ru.stali.secretary.transfer.UserDTO;

import java.util.List;

/**
 * 03.05.2020
 * UsersController
 *
 * @author Havlong
 * @version v1.0
 */
@RestController
public class UsersController {

    private final UserRepository userRepository;

    public UsersController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/users")
    public List<UserDTO> getUsersList() {
        return UserDTO.from(userRepository.findAll());
    }
}
