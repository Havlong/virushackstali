package ru.stali.secretary.transfer;

import lombok.*;

/**
 * 04.05.2020
 * EndpointDTO
 *
 * @author Havlong
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class EndpointDTO {
    private String value;
}
