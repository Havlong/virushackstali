package ru.stali.secretary.transfer;

import lombok.*;

/**
 * 04.05.2020
 * MessageDTO
 *
 * @author Havlong
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class MessageDTO {
    private String token;
    private String author;
    private String value;
}
