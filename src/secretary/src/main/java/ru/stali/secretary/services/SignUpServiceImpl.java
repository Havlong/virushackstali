package ru.stali.secretary.services;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.stali.secretary.models.User;
import ru.stali.secretary.repositories.UserRepository;
import ru.stali.secretary.transfer.CredentialsDTO;

/**
 * 03.05.2020
 * SignUpServiceImpl
 *
 * @author Havlong
 * @version v1.0
 */
@Service
public class SignUpServiceImpl implements SignUpService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public SignUpServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public boolean signUp(CredentialsDTO credentialsDTO) {
        String hashPassword = passwordEncoder.encode(credentialsDTO.getPassword());
        User user = User.builder().userName(credentialsDTO.getLogin()).hashPassword(hashPassword).build();
        userRepository.save(user);
        return user.getId() != null;
    }
}
