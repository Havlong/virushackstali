package ru.stali.secretary.models;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * 04.05.2020
 * Stream
 *
 * @author Havlong
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
@EqualsAndHashCode
@Document
public class Stream {
    @Id
    String id;
    @DBRef
    User owner;
    boolean isWritable;

    List<String> endpoints;
}
