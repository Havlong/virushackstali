package ru.stali.secretary.transfer;

import lombok.*;

/**
 * 03.05.2020
 * UserDTO
 *
 * @author Havlong
 * @version v1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CredentialsDTO {
    private String login;
    private String password;
}
