package ru.stali.secretary.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.stali.secretary.models.Stream;

/**
 * 04.05.2020
 * StreamRepository
 *
 * @author Havlong
 * @version v1.0
 */
public interface StreamRepository extends MongoRepository<Stream, String> {
}
