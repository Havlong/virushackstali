package ru.stali.secretary.services;

import org.springframework.stereotype.Service;
import ru.stali.secretary.models.Stream;
import ru.stali.secretary.transfer.EventDTO;

import javax.websocket.Session;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 04.05.2020
 * StreamServiceInMemoryImpl
 *
 * @author Havlong
 * @version v1.0
 */
@Service
public class StreamServiceInMemoryImpl implements StreamService {
    private final Map<Stream, List<Session>> sessions = new HashMap<>();

    @Override
    public List<Session> getSessionsForStream(Stream stream) {
        if (sessions.containsKey(stream)) {
            return sessions.get(stream);
        }
        throw new IllegalArgumentException("Stream is not open");
    }

    @Override
    public void registerSessionForStream(Stream stream, Session session) {
        if (sessions.containsKey(stream)) {
            sessions.get(stream).add(session);
        } else {
            throw new IllegalArgumentException("Stream is not open");
        }
    }

    @Override
    public void closeSessionForStream(Stream stream, Session session) {
        if (sessions.containsKey(stream)) {
            int index = sessions.get(stream).lastIndexOf(session);
            try {
                sessions.get(stream).get(index).close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            sessions.get(stream).remove(index);
        }
    }

    @Override
    public void sendMessageToStream(Stream stream, EventDTO eventDTO) {
        if (sessions.containsKey(stream)) {
            sessions.get(stream).forEach(session -> session.getAsyncRemote().sendObject(eventDTO));
            sendMessageToStreamAPIs(stream, eventDTO);
        } else {
            throw new IllegalArgumentException("Stream is not open");
        }
    }

    @Override
    public void startStream(Stream stream) {
        if (!sessions.containsKey(stream)) {
            sessions.put(stream, new ArrayList<>());
        } else {
            throw new IllegalArgumentException("Stream is already opened");
        }
    }

    @Override
    public void closeStream(Stream stream) {
        if (sessions.containsKey(stream)) {
            sessions.get(stream).forEach(session -> {
                try {
                    session.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            sessions.remove(stream);
        }
    }

    public void sendMessageToStreamAPIs(Stream stream, EventDTO eventDTO) {

    }
}
