package ru.stali.secretary.security.provider;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import ru.stali.secretary.models.Token;
import ru.stali.secretary.repositories.TokenRepository;
import ru.stali.secretary.security.token.TokenAuthentication;

/**
 * 03.05.2020
 * TokenAuthenticationProvider
 *
 * @author Havlong
 * @version v1.0
 */
@Component
public class TokenAuthenticationProvider implements AuthenticationProvider {

    private final TokenRepository tokenRepository;
    private final UserDetailsService userDetailsService;

    public TokenAuthenticationProvider(TokenRepository tokenRepository, @Qualifier("userDetailsServiceImpl") UserDetailsService userDetailsService) {
        this.tokenRepository = tokenRepository;
        this.userDetailsService = userDetailsService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        TokenAuthentication tokenAuthentication = (TokenAuthentication) authentication;

        Token tokenCandidate = tokenRepository.findTokenByValue(tokenAuthentication.getName());

        if (tokenCandidate == null) {
            throw new AuthenticationCredentialsNotFoundException("Bad token");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(tokenCandidate.getUser().getUserName());
        tokenAuthentication.setUserDetails(userDetails);
        tokenAuthentication.setAuthenticated(true);
        return tokenAuthentication;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return TokenAuthentication.class.equals(aClass);
    }
}
