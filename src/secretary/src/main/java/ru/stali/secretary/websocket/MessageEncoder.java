package ru.stali.secretary.websocket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.stali.secretary.transfer.EventDTO;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 * 04.05.2020
 * MessageEncoder
 *
 * @author Havlong
 * @version v1.0
 */
public class MessageEncoder implements Encoder.Text<EventDTO> {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String encode(EventDTO eventDTO) throws EncodeException {
        try {
            return objectMapper.writeValueAsString(eventDTO);
        } catch (JsonProcessingException e) {
            throw new EncodeException(eventDTO, "Invalid object values");
        }
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
