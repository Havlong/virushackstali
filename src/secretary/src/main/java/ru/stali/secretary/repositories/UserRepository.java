package ru.stali.secretary.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.stali.secretary.models.User;

/**
 * 03.05.2020
 * UserRepository
 *
 * @author Havlong
 * @version v1.0
 */
public interface UserRepository extends MongoRepository<User, String> {

    User findUserByUserName(String userName);


}
