package ru.stali.secretary.security.details;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.stali.secretary.models.User;
import ru.stali.secretary.repositories.UserRepository;

/**
 * 03.05.2020
 * UserDetailsServiceImpl
 *
 * @author Havlong
 * @version v1.0
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findUserByUserName(s);
        if (user == null) {
            throw new UsernameNotFoundException("Username does not exist");
        }
        return new UserDetailsImpl(user);
    }
}
