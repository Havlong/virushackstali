package ru.stali.secretary.controllers;

import org.springframework.stereotype.Component;
import ru.stali.secretary.CustomContext;
import ru.stali.secretary.models.Stream;
import ru.stali.secretary.repositories.StreamRepository;
import ru.stali.secretary.services.StreamService;
import ru.stali.secretary.transfer.EventDTO;
import ru.stali.secretary.transfer.MessageDTO;
import ru.stali.secretary.websocket.MessageDecoder;
import ru.stali.secretary.websocket.MessageEncoder;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

/**
 * 04.05.2020
 * StreamEndpoint
 *
 * @author Havlong
 * @version v1.0
 */
@Component
@ServerEndpoint(value = "/stream/{streamId}", encoders = MessageEncoder.class, decoders = MessageDecoder.class)
public class StreamEndpoint {

    private final StreamService streamService;
    private final StreamRepository streamRepository;

    public StreamEndpoint() {
        streamService = CustomContext.getBean(StreamService.class);
        streamRepository = CustomContext.getBean(StreamRepository.class);
    }

    @OnOpen
    public void onOpen(Session session, @PathParam("streamId") String streamId) {
        Stream stream = streamRepository.findById(streamId).orElse(null);
        if (stream == null) {
            try {
                session.close(new CloseReason(CloseReason.CloseCodes.VIOLATED_POLICY, "Bad credentials"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        streamService.registerSessionForStream(stream, session);
    }

    @OnMessage
    public void onMessage(MessageDTO messageDTO, @PathParam("streamId") String streamId) {
        Stream stream = streamRepository.findById(streamId).orElse(null);
        if (stream != null && stream.isWritable())
            streamService.sendMessageToStream(stream, EventDTO.from(messageDTO));
    }

    @OnClose
    public void onClose(Session session, @PathParam("streamId") String streamId) {
        Stream stream = streamRepository.findById(streamId).orElse(null);
        streamService.closeSessionForStream(stream, session);
    }
}
