package ru.stali.secretary.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.stali.secretary.models.Stream;
import ru.stali.secretary.models.Token;
import ru.stali.secretary.models.User;
import ru.stali.secretary.repositories.StreamRepository;
import ru.stali.secretary.repositories.TokenRepository;
import ru.stali.secretary.services.StreamService;
import ru.stali.secretary.transfer.EndpointDTO;

import javax.websocket.Session;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 04.05.2020
 * StreamController
 *
 * @author Havlong
 * @version v1.0
 */
@RestController
public class StreamController {
    private final StreamRepository streamRepository;
    private final StreamService streamService;
    private final TokenRepository tokenRepository;

    public StreamController(StreamRepository streamRepository, StreamService streamService, TokenRepository tokenRepository) {
        this.streamRepository = streamRepository;
        this.streamService = streamService;
        this.tokenRepository = tokenRepository;
    }

    @PostMapping("/streamer/create")
    public ResponseEntity<String> createStream() {
        String tokenValue = SecurityContextHolder.getContext().getAuthentication().getName();
        Token token = tokenRepository.findTokenByValue(tokenValue);
        User creator = token.getUser();
        Stream stream = Stream.builder().endpoints(new ArrayList<>()).owner(creator).isWritable(false).build();
        streamRepository.save(stream);
        streamService.startStream(stream);
        return ResponseEntity.ok(stream.getId());
    }

    @PostMapping("/streamer/close/{streamId}")
    public ResponseEntity<String> closeStream(@PathVariable String streamId) {
        Stream stream = streamRepository.findById(streamId).orElse(null);
        streamService.closeStream(stream);
        if (stream == null) {
            throw new IllegalArgumentException("Stream doesn't exist");
        }
        streamRepository.save(stream);
        return ResponseEntity.ok("Stream closed");
    }

    @PostMapping("/streamer/addEndpoint/{streamId}")
    public void addEndpoint(@PathVariable String streamId, @RequestBody EndpointDTO endpointDTO) {
        Stream stream = streamRepository.findById(streamId).orElse(null);
        if (stream == null) {
            throw new IllegalArgumentException("Stream doesn't exist");
        }
        stream.getEndpoints().add(endpointDTO.getValue());
        streamRepository.save(stream);
    }

    @PostMapping("/streamer/writable/{streamId}")
    public void setWritable(@PathVariable String streamId) {
        Stream stream = streamRepository.findById(streamId).orElse(null);
        if (stream == null) {
            throw new IllegalArgumentException("Stream doesn't exist");
        }
        stream.setWritable(true);
        streamRepository.save(stream);
    }

    @PostMapping("/streamer/readable/{streamId}")
    public void setReadable(@PathVariable String streamId) {
        Stream stream = streamRepository.findById(streamId).orElse(null);
        if (stream == null) {
            throw new IllegalArgumentException("Stream doesn't exist");
        }
        stream.setWritable(true);
        streamRepository.save(stream);
    }

    @PostMapping("/streamapi/check/{streamId}")
    public ResponseEntity<String> check(@PathVariable String streamId) {
        Stream stream = streamRepository.findById(streamId).orElse(null);
        if (stream == null) {
            return ResponseEntity.ok("Failure");
        }
        List<Session> sessionList = streamService.getSessionsForStream(stream);
        if (sessionList != null) {
            return ResponseEntity.ok("Ok");
        }
        return ResponseEntity.ok("Failure");
    }

    @GetMapping("/streamapi/all")
    public List<EndpointDTO> allTokens(String secretKey) {
        if (secretKey.equals("486b3391-d85f-4df0-af73-67388e0b5cc6"))
            return streamRepository.findAll().stream().map(stream -> new EndpointDTO(stream.getId())).collect(Collectors.toList());
        else throw new IllegalStateException("secret key doesn't match");
    }
}
