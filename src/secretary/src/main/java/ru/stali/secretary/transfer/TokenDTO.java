package ru.stali.secretary.transfer;

import lombok.*;
import ru.stali.secretary.models.Token;

/**
 * 03.05.2020
 * TokenDTO
 *
 * @author Havlong
 * @version v1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TokenDTO {
    private String value;

    public static TokenDTO from(Token token) {
        return new TokenDTO(token.getValue());
    }
}
