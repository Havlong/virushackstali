package ru.stali.secretary.transfer;

import lombok.*;
import ru.stali.secretary.models.User;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 03.05.2020
 * UserDTO
 *
 * @author Havlong
 * @version v1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class UserDTO {
    private String login;

    public static UserDTO from(User user) {
        return UserDTO.builder().login(user.getUserName()).build();
    }

    public static List<UserDTO> from(List<User> users) {
        return users.stream().map(UserDTO::from).collect(Collectors.toList());
    }
}
