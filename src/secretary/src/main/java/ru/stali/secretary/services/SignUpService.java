package ru.stali.secretary.services;

import ru.stali.secretary.transfer.CredentialsDTO;

/**
 * 03.05.2020
 * SignUpService
 *
 * @author Havlong
 * @version v1.0
 */
public interface SignUpService {

    boolean signUp(CredentialsDTO credentialsDTO);
}
