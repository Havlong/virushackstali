import pyaudio
import time

import numpy as np

audio = pyaudio.PyAudio()

microphone_device = audio.get_device_info_by_index(0)
output_device = audio.get_device_info_by_index(1)
virtual_device = audio.get_device_info_by_index(2)

FORMAT = pyaudio.paInt16
CHUNK_SIZE = 1024  # Maybe make smaller for windows

incoming_stream = None
microphone_stream = None
re_stream = None

app = None


# for i in range(0, audio.get_device_count()):
#     info = audio.get_device_info_by_index(i)
#     print(info)
#     hostAPI = audio.get_host_api_info_by_index(int(info['hostApi']))
#     print(hostAPI)


def get_devices_names() -> list:
    names = []
    for i in range(0, audio.get_device_count()):
        info = audio.get_device_info_by_index(i)
        names.append(info['name'])
    return names


def set_microphone_device(index: int) -> bool:
    global microphone_device
    info = audio.get_device_info_by_index(index)
    if info['maxInputChannels'] > 0 and info['maxOutputChannels'] == 0:
        microphone_device = info
        return True


def set_output_device(index: int) -> bool:
    global output_device
    info = audio.get_device_info_by_index(index)
    if info['maxOutputChannels'] > 0 and info['maxInputChannels'] == 0:
        output_device = info
        return True


def set_virtual_device(index: int) -> bool:
    global virtual_device
    info = audio.get_device_info_by_index(index)
    if info['maxOutputChannels'] > 0:
        virtual_device = info
        return True


def start_streaming():
    global microphone_stream, incoming_stream, re_stream
    re_stream = audio.open(format=FORMAT,
                           channels=int(output_device['maxOutputChannels']),
                           rate=int(output_device['defaultSampleRate']),
                           output=True,
                           output_device_index=int(output_device['index']),
                           frames_per_buffer=CHUNK_SIZE)
    incoming_stream = audio.open(format=FORMAT,
                                 channels=int(output_device['maxOutputChannels']),
                                 rate=int(virtual_device['defaultSampleRate']),
                                 input=True,
                                 input_device_index=int(virtual_device['index']),
                                 frames_per_buffer=CHUNK_SIZE, stream_callback=resend_callback)
    microphone_stream = audio.open(format=FORMAT,
                                   channels=int(microphone_device['maxInputChannels']),
                                   rate=int(microphone_device['defaultSampleRate']),
                                   input=True,
                                   input_device_index=int(microphone_device['index']),
                                   frames_per_buffer=CHUNK_SIZE, stream_callback=microphone_callback)


def microphone_callback(in_data, frame_count, time_info, status):
    if microphone_stream.is_active() and status == 0:
        a = np.frombuffer(in_data, np.uint16)
        # send 'a' to Stas
        # app.add_new_text(  строка )
        return None, pyaudio.paNoError
    else:
        return None, pyaudio.paAbort

def resend_callback(in_data, frame_count, time_info, status):
    if re_stream.is_active() and status == 0:
        re_stream.write(in_data)
        a = np.frombuffer(in_data, np.uint16)
        # send 'a' to Stas
        # app.add_new_text(  строка )
        return None, pyaudio.paNoError
    else:
        return None, pyaudio.paAbort

def test_run():
    start_streaming()
    while re_stream.is_active():
        time.sleep(0.1)
    print('krlr')


def close_streams():
    microphone_stream.stop_stream()
    incoming_stream.stop_stream()
    re_stream.stop_stream()
    microphone_stream.close()
    incoming_stream.close()
    re_stream.close()
    # audio.terminate()


# Just for test
# test_run()
