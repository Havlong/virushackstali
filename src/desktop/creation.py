import sys

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

import recorder

class Application(QWidget):

    def __init__(self):
        super().__init__()
        recorder.app = self
        self.setWindowTitle("secREC")
        self.setFixedSize(1066, 600)
        with open("resources/window.css", "r") as fh:
            self.setStyleSheet(fh.read())
        self._create_ui()

    def _create_ui(self):

        self.input_state = False
        self.output_state = False
        self.virtual_state = False
        self.file_state = False
        self.dir = QDir("/не выбрана")
        self.mainLayout = QHBoxLayout()
        self.mainLayout.setContentsMargins(20, 20, 20, 20)

        # Information and buttons (Left)

        self.leftLayout = QVBoxLayout()
        logo = QLabel("secREC")
        logo.setStyleSheet("font: Bold; font-size: 130px;")
        self.leftLayout.addWidget(logo)
        # self.leftLayout.addSpacerItem(QSpacerItem(0, 0, 0, QSizePolicy.Expanding))

        self.buttons = QHBoxLayout()
        self.buttons.addSpacerItem(QSpacerItem(40, 0))

        self.default_stage()
        self.start_button.setDisabled(True)

        self.leftLayout.addLayout(self.buttons)
        self.leftLayout.addSpacerItem(QSpacerItem(0, 0, 0, QSizePolicy.MinimumExpanding))

        # Select devices

        devices_info = QHBoxLayout()
        devices_info.addWidget(QLabel("Микрофон"))
        devices_info.addWidget(QLabel("Звуковой выход"))
        devices_info.addWidget(QLabel("Виртуальный кабель"))

        getted_devices = ["не выбрано"].__add__(recorder.get_devices_names())
        # getted_devices.insert(0, '')

        devices = QHBoxLayout()

        self.input_device = QComboBox(self)
        self.input_device.setEditable(False)
        # self.input_device.editTextChanged.connect(self.findText)
        self.input_device.addItems(getted_devices)
        devices.addWidget(self.input_device)
        self.input_device.activated[str].connect(self.check_input)

        self.output_device = QComboBox(self)
        self.output_device.setEditable(False)
        # self.output_device.editTextChanged.connect(self.findText)
        self.output_device.addItems(getted_devices)
        devices.addWidget(self.output_device)
        self.output_device.activated[str].connect(self.check_output)

        self.virtual_device = QComboBox(self)
        self.virtual_device.setEditable(False)
        # self.virtual_device.editTextChanged.connect(self.findText)
        self.virtual_device.addItems(getted_devices)
        devices.addWidget(self.virtual_device)
        self.virtual_device.activated[str].connect(self.check_virtual)

        self.leftLayout.addLayout(devices_info)
        self.leftLayout.addLayout(devices)

        self.mainLayout.addLayout(self.leftLayout, Qt.AlignLeft)

        # Scroll (Right)

        textScroll = QScrollArea()
        self.textEdit = QTextEdit()
        self.textEdit.setReadOnly(True)

        textScroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        textScroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        textScroll.setWidgetResizable(True)
        textScroll.setWidget(self.textEdit)
        textScroll.setMinimumWidth(500)
        self.mainLayout.addSpacerItem(QSpacerItem(0, 0, QSizePolicy.MinimumExpanding))
        self.mainLayout.addWidget(textScroll)

        # Setup in window
        self.setLayout(self.mainLayout)

    def check_input(self):
        if self.input_device.currentIndex() == 0: return
        self.input_state = recorder.set_microphone_device(self.input_device.currentIndex() - 1)
        if self.input_state:
            self.showStartButton()
        if self.input_state is None:
            self.input_device.setCurrentIndex(0)

    def check_output(self):
        if self.output_device.currentIndex() == 0: return
        self.output_state = recorder.set_output_device(self.output_device.currentIndex() - 1)
        if self.output_state:
            self.showStartButton()
        if self.output_state is None:
            self.output_device.setCurrentIndex(0)

    def check_virtual(self):
        if self.virtual_device.currentIndex() == 0: return
        self.virtual_state = recorder.set_virtual_device(self.virtual_device.currentIndex() - 1)
        if self.virtual_state:
            self.showStartButton()
        if self.virtual_state is None:
            self.virtual_device.setCurrentIndex(0)

    def default_stage(self):
        self.folder_main = QLabel("ПАПКА ДЛЯ ЗАПИСИ")
        self.folder_main.setStyleSheet("font-size: 30px;")
        self.leftLayout.insertWidget(1, self.folder_main)
        self.folder_path = QLabel(self.dir.absolutePath())
        self.folder_path.setStyleSheet("font-size: 15px;")
        self.leftLayout.insertWidget(2, self.folder_path)
        self.directory_button = QPushButton('ВЫБРАТЬ ПАПКУ', self)
        self.start_button = QPushButton('НАЧАТЬ ЗАПИСЬ', self)

        self.start_button.clicked.connect(self.start_button_press)
        self.directory_button.clicked.connect(self.openFileNamesDialog)

        self.buttons.insertWidget(0, self.directory_button)
        self.buttons.insertWidget(1, self.start_button)

    def start_button_press(self):
        self.input_device.setEnabled(False)
        self.output_device.setEnabled(False)
        self.virtual_device.setEnabled(False)
        self.folder_main.hide()
        self.folder_path.hide()
        self.leftLayout.removeWidget(self.folder_main)
        self.leftLayout.removeWidget(self.folder_path)
        self.start_button.hide()
        self.directory_button.hide()
        self.buttons.removeWidget(self.start_button)
        self.buttons.removeWidget(self.directory_button)

        self.record_info = QLabel("ИДЕТ ЗАПИСЬ")
        self.record_info.setStyleSheet("font-size: 30px;")
        self.leftLayout.insertWidget(1, self.record_info)

        self.record_time = QLabel("00:00:00")
        self.record_time.setStyleSheet("font-size: 15px;")
        self.leftLayout.insertWidget(2, self.record_time)

        self.file_info = QLabel("ФАЙЛ")
        self.file_info.setStyleSheet("font-size: 30px;")
        self.leftLayout.insertWidget(3, self.file_info)

        self.file = QLabel(self.current_file)
        self.file.setStyleSheet("font-size: 15px;")
        self.leftLayout.insertWidget(4, self.file)

        self.stop_button = QPushButton("ОСТАНОВИТЬ")
        self.stop_button.clicked.connect(self.stop_button_press)
        self.stop_button.setObjectName("stop")
        self.buttons.insertWidget(0, self.stop_button)

        recorder.start_streaming()

        # self.leftLayout.removeItem(self.buttons)

    def stop_button_press(self):
        self.file_state = False
        recorder.start_streaming()
        self.record_info.hide()
        self.record_time.hide()
        self.file_info.hide()
        self.file.hide()
        self.input_device.setEnabled(True)
        self.output_device.setEnabled(True)
        self.virtual_device.setEnabled(True)
        self.leftLayout.removeWidget(self.record_info)
        self.leftLayout.removeWidget(self.record_time)
        self.leftLayout.removeWidget(self.file_info)
        self.leftLayout.removeWidget(self.file)
        self.stop_button.hide()
        self.buttons.removeWidget(self.stop_button)
        self.default_stage()

    def add_new_text(self, text: str):
        self.textEdit.append(text)
        with open(self.current_file, 'a') as the_file:
            the_file.write(text + '\n')

    def findText(self, s):
        index = self.combo.findText(s)
        if index > -1:
            self.combo.setCurrentIndex(index)

    def showStartButton(self):
        if self.input_state and self.output_state and self.virtual_state and self.file_state:
            self.start_button.setDisabled(False)

    def openFileNamesDialog(self):
        options = QFileDialog.Options()
        direct = QFileDialog.getExistingDirectory(options=options)
        if len(direct) > 0:
            self.folder_path.setText(direct)
            self.directory_button.setText('ИЗМЕНИТЬ ПАПКУ')
            self.dir = QDir(direct)
            self.current_file = self.dir.filePath("speech-" + str(self.dir.count()) + ".txt")
            self.file_state = True
            self.showStartButton()

    def closeEvent(self, event):
        recorder.close_streams()
        pass


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Application()
    ex.show()
    sys.exit(app.exec_())
