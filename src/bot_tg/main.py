import asyncio
import websockets
from json import loads
import requests

offset = 365180577
token = '' # токен бота
prx = "51.158.68.68:8811" # http прокси с https://www.firexproxy.com/
proxies = {
 'http': 'http://' + prx,
 'https': 'http://' + prx,
}

users = dict()
users['test'] = []

def check_users(): # проверка полученных ботом сообщений, доступ пользователей к стриму
    global offset
    global users
    r = loads(requests.get('https://api.telegram.org/' + token + "/getUpdates?offset=" + str(offset), proxies=proxies).content)
    for message in r['result']:
        on_stream = False
        offset = int(message['update_id']) + 1
        user_id = str(message['message']['from']['id'])
        # print(message['message']['text'])
        for i in users:
            if users.get(i).__contains__(user_id):
                on_stream = True
                break

        if str(message['message']['text']) != '/stop' and on_stream:
            bot_send(user_id, 'Ваш запрос не распознан. Введите /stop, чтобы остановить трансляцию.')
        else:
            if len(str(message['message']['text'])) == len('5eaf084347de8f079d8c3ee2'):
                check = requests.post('http://93.124.69.158:8000/streamapi/check/' + str(message['message']['text'])).content
                print(str(check))
                if str(check) == 'b\'Ok\'':
                    users.setdefault(str(message['message']['text']))
                    try:
                        users[str(message['message']['text'])].append(str(user_id))
                    except:
                        users[str(message['message']['text'])] = [str(user_id)]
                    bot_send(user_id, 'Вы успешно добавлены на сервер трансляции.')
                else:
                    bot_send(user_id, 'Такой трансляции не существует!')
            else:
                if str(message['message']['text']) == '/stop':
                    flag = True
                    for i in users:
                        if users.get(i).__contains__(user_id):
                            users.get(i).remove(user_id)
                            bot_send(user_id, 'Трансляция отключена.')
                            flag = False
                    if flag:
                        bot_send(user_id, 'Вы не подключены ни к одной трансляции.')
                else:
                    bot_send(user_id, 'Ваш запрос не распознан, попробуйте ещё раз.')

def bot_send(user_id, message): # отправка сообщения message пользователю с id = user_id
    r = requests.post('https://api.telegram.org/' + token + "/sendMessage?chat_id=" + str(user_id) + "&text=" + str(message), proxies=proxies)

async def publish_text(stream_id, author, value): # публикация текста, полученного со стрима
    message = author + "said: " + value
    for user_id in users[stream_id]:
        bot_send(user_id, message)

async def client(stream_id): # клиент, проверяющий обновления на стриме
    uri = "ws://93.124.69.158:8000/stream/" + stream_id
    async with websockets.connect(uri) as websocket:
        while True:
            message = await websocket.recv()
            info = loads(message)
            # await publish_text(stream_id, info.author, info.value)
            await publish_text(info["author"], info["value"])

if __name__ == '__main__':

    while True:
        check_users()

    # asyncio.get_event_loop().run_until_complete(client())